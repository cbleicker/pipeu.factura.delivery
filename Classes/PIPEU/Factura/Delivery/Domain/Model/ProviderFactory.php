<?php
/*                                                                        *
 * This script belongs to the TYPO3 Flow framework.                       *
 *                                                                        *
 * It is free software; you can redistribute it and/or modify it under    *
 * the terms of the GNU Lesser General Public License, either version 3   *
 * of the License, or (at your option) any later version.                 *
 *                                                                        *
 * The TYPO3 project - inspiring people to share!                         *
 *                                                                        */

namespace PIPEU\Factura\Delivery\Domain\Model;

use Doctrine\Common\Collections\ArrayCollection;
use PIPEU\Factura\Delivery\Domain\Model\Exceptions;
use PIPEU\Factura\Delivery\Domain\Model\Provider\Dummy;
use TYPO3\Flow\Annotations as Flow;
use TYPO3\Flow\Configuration\ConfigurationManager;
use TYPO3\Flow\Object\ObjectManagerInterface;
use TYPO3\Flow\Reflection\ReflectionService;

/**
 * Class ProviderFactory
 *
 * @package PIPEU\Factura\Delivery\Domain\Model
 * @Flow\Scope("singleton")
 */
class ProviderFactory {

	const CLASSNAME_TO_REFLECT = 'PIPEU\Factura\Delivery\Domain\Model\InterfaceProvider';

	/**
	 * @var ConfigurationManager
	 * @Flow\Inject
	 * @Flow\Transient
	 */
	protected $configurationManager;

	/**
	 * @Flow\Inject
	 * @var ObjectManagerInterface
	 */
	protected $objectManager;

	/**
	 * @Flow\Inject
	 * @var ReflectionService
	 */
	protected $reflectionService;

	/**
	 * @param $type
	 * @return InterfaceProvider
	 * @throws Exceptions\NoProviderFoundException
	 */
	public function create($type) {
		$providers  = $this->getAllProvidersByHighestPriority();

		$typeFilter = function (InterfaceProvider $provider) use ($type) {
			return $provider->getType() === $type;
		};

		$possibleProvidersForGivenType = $providers->filter($typeFilter);

		if ($possibleProvidersForGivenType->count() === 0) {
			throw new Exceptions\NoProviderFoundException('Could not find any delivery provider for given type "' . $type . '"', 1387363908);
		}

		/** @var InterfaceProvider $provider */
		$provider = $possibleProvidersForGivenType->first();
		$provider->setConfiguration($this->getConfigurationForType($type));

		return $provider;
	}

	/**
	 * @param string $type
	 * @return mixed
	 */
	protected function getConfigurationForType($type) {
		return $this->configurationManager->getConfiguration('Delivery', $type);
	}

	/**
	 * @return ArrayCollection<InterfaceProvider>
	 */
	public function getAllProvidersByHighestPriority() {
		$availableProvidersByHighestPriority = new ArrayCollection();
		$availableProviders                  = $this->reflectionService->getAllImplementationClassNamesForInterface(self::CLASSNAME_TO_REFLECT);
		$providerIteration                   = new \ArrayIterator($availableProviders);
		$providerIteration->rewind();

		while ($providerIteration->valid()) {

			/** @var InterfaceProvider $currentProvider */
			$currentProvider = $this->objectManager->get($providerIteration->current());
			$currentProvider->setConfiguration($this->getConfigurationForType($currentProvider->getType()));

			if($currentProvider instanceof Dummy){
				$providerIteration->next();
				continue;
			}

			if ($availableProvidersByHighestPriority->offsetExists($currentProvider->getType())) {
				/** @var InterfaceProvider $currentlyRegisteredProvider */
				$currentlyRegisteredProvider = $availableProvidersByHighestPriority->offsetGet($currentProvider->getType());
				if ($currentProvider->getPriority() > $currentlyRegisteredProvider->getPriority()) {
					$availableProvidersByHighestPriority->offsetSet($providerIteration->key(), $currentProvider);
				}
			} else {
				$availableProvidersByHighestPriority->offsetSet($providerIteration->key(), $currentProvider);
			}
			$providerIteration->next();
		}

		return $availableProvidersByHighestPriority;
	}
}
