<?php
/*                                                                        *
 * This script belongs to the TYPO3 Flow framework.                       *
 *                                                                        *
 * It is free software; you can redistribute it and/or modify it under    *
 * the terms of the GNU Lesser General Public License, either version 3   *
 * of the License, or (at your option) any later version.                 *
 *                                                                        *
 * The TYPO3 project - inspiring people to share!                         *
 *                                                                        */

namespace PIPEU\Factura\Delivery\Domain\Model\Packaging\Service\Interfaces;

use Doctrine\Common\Collections\Collection;
use PIPEU\Factura\Delivery\Settings\Settings;
use DVDoug\BoxPacker\Box as BoxInterface;
use DVDoug\BoxPacker\ItemList;
use DVDoug\BoxPacker\PackedBox;

/**
 * Class PackageFactoryInterface
 *
 * @package PIPEU\Factura\Delivery\Domain\Model\Packaging\Service\Interfaces
 */
interface PackageFactoryInterface {

	/**
	 * @param Collection $facturaItems
	 * @return BoxInterface
	 */
	public function findBestPackagingForFacturaItems(Collection $facturaItems);

	/**
	 * @param ItemList $items
	 * @return BoxInterface
	 */
	public function findBestPackaging(ItemList $items);

	/**
	 * @param BoxInterface $testBox
	 * @param ItemList $items
	 * @return PackedBox
	 */
	public function packIt(BoxInterface $testBox, ItemList $items);

	/**
	 * @return Settings
	 */
	public function getSettings();
}