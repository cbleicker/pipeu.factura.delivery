<?php
/*                                                                        *
 * This script belongs to the TYPO3 Flow framework.                       *
 *                                                                        *
 * It is free software; you can redistribute it and/or modify it under    *
 * the terms of the GNU Lesser General Public License, either version 3   *
 * of the License, or (at your option) any later version.                 *
 *                                                                        *
 * The TYPO3 project - inspiring people to share!                         *
 *                                                                        */

namespace PIPEU\Factura\Delivery\Domain\Model\Packaging\Service;

use Doctrine\Common\Collections\Collection;
use DVDoug\BoxPacker\ItemList;
use DVDoug\BoxPacker\Box as BoxInterface;
use PIPEU\Factura\Delivery\Domain\Model\Box as Packaging;
use DVDoug\BoxPacker\Packer;
use DVDoug\BoxPacker\PackedBox;
use PIPEU\Factura\Delivery\Domain\Model\Item;
use PIPEU\Factura\Delivery\Domain\Model\Packaging\Service\Interfaces\PackageFactoryInterface;
use PIPEU\Factura\Delivery\Settings\Settings;
use PIPEU\Factura\Domain\Interfaces\InterfaceFacturaDocument;
use PIPEU\Factura\Domain\Interfaces\InterfaceFacturaItem;
use PIPEU\Factura\Domain\Model\Documents\Items\PhysicalProduct;
use TYPO3\Flow\Annotations as Flow;
use TYPO3\Flow\Utility\Arrays;

/**
 * Class PackageFactory
 *
 * @package PIPEU\Factura\Delivery\Domain\Model\Packaging\Service
 * @Flow\Scope("singleton")
 */
class PackageFactory implements PackageFactoryInterface {

	/**
	 * @var Settings
	 */
	protected $settings;

	/**
	 * @param array $settings
	 * @return void
	 */
	public function injectSettings(array $settings = array()) {
		$this->settings = new Settings($settings);
	}

	/**
	 * @return Settings
	 */
	public function getSettings() {
		return $this->settings;
	}

	/**
	 * @param Collection $facturaItems
	 * @return BoxInterface
	 */
	public function findBestPackagingForFacturaItems(Collection $facturaItems) {
		$itemList = new ItemList();
		$facturaItems->forAll($this->addFacturaItemToList($itemList));
		return $this->findBestPackaging($itemList);
	}

	/**
	 * @param ItemList $itemList
	 * @return \Closure
	 */
	protected function addFacturaItemToList(ItemList $itemList) {
		return function ($index, InterfaceFacturaItem $facturaItem) use ($itemList) {
			if ($facturaItem instanceof PhysicalProduct) {
				for ($i = 0; $i < (integer)(string)$facturaItem->getUnit(); $i++) {
					$itemList->insert(new Item($facturaItem->getTitle(), $facturaItem->getWidth(), $facturaItem->getLength(), $facturaItem->getDepth(), $facturaItem->getUnitWeight()->getValue()));
				}
			}
		};
	}

	/**
	 * @param ItemList $items
	 * @return BoxInterface
	 */
	public function findBestPackaging(ItemList $items) {

		$packagingBoxes = (array)$this->getSettings()->getValueByPath('packaging.boxes');
		foreach ($packagingBoxes as $title => $dimension) {
			$dimensionArray = (array)$dimension;
			$outerWidth = (integer)Arrays::getValueByPath($dimensionArray, 'outerWidth');
			$outerLength = (integer)Arrays::getValueByPath($dimensionArray, 'outerLength');
			$outerDepth = (integer)Arrays::getValueByPath($dimensionArray, 'outerDepth');
			$innerWidth = (integer)Arrays::getValueByPath($dimensionArray, 'innerWidth');
			$innerLength = (integer)Arrays::getValueByPath($dimensionArray, 'innerLength');
			$innerDepth = (integer)Arrays::getValueByPath($dimensionArray, 'innerDepth');
			$emptyWeight = (integer)Arrays::getValueByPath($dimensionArray, 'emptyWeight');
			$maxWeight = (integer)Arrays::getValueByPath($dimensionArray, 'maxWeight');
			$unitPrice = (integer)Arrays::getValueByPath($dimensionArray, 'price');

			$box = new Packaging((string)$title, $outerWidth, $outerLength, $outerDepth, $emptyWeight, $innerWidth, $innerLength, $innerDepth, $maxWeight, $unitPrice);
			$this->packIt($box, $items);

			if ($items->count() === 0) {
				return $box;
			}
		}
	}

	/**
	 * @param BoxInterface $box
	 * @param ItemList $items
	 * @return PackedBox
	 */
	public function packIt(BoxInterface $box, ItemList $items) {
		$packer = new Packer();
		$packedBox = $packer->packIntoBox($box, $items);
		return $packedBox;
	}
}
