<?php
/*                                                                        *
 * This script belongs to the TYPO3 Flow framework.                       *
 *                                                                        *
 * It is free software; you can redistribute it and/or modify it under    *
 * the terms of the GNU Lesser General Public License, either version 3   *
 * of the License, or (at your option) any later version.                 *
 *                                                                        *
 * The TYPO3 project - inspiring people to share!                         *
 *                                                                        */

namespace PIPEU\Factura\Delivery\Domain\Model\Provider;

use PIPEU\Geo\Domain\Model\Country;
use PIPEU\Factura\Domain\Model\Tax;
use PIPEU\Factura\Domain\Model\Weight;
use PIPEU\Factura\Domain\Model\Money;
use PIPEU\Factura\Delivery\Domain\Model\AbstractProvider;

/**
 * Class Dummy
 *
 * @package PIPEU\Factura\Delivery\Domain\Model\Provider
 */
class Dummy extends AbstractProvider {

	const PRIORITY = -1000000;

	/**
	 * @param mixed $object
	 * @return boolean
	 */
	public function isProvided($object) {
		return FALSE;
	}

	/**
	 * @param mixed $object
	 * @return Money
	 */
	public function getUnitPrice($object) {
		return new Money(0);
	}

	/**
	 * @return Tax
	 */
	public function getTax() {
		return new Tax(0);
	}
}
