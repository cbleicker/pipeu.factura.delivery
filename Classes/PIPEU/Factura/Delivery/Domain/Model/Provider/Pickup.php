<?php
/*                                                                        *
 * This script belongs to the TYPO3 Flow framework.                       *
 *                                                                        *
 * It is free software; you can redistribute it and/or modify it under    *
 * the terms of the GNU Lesser General Public License, either version 3   *
 * of the License, or (at your option) any later version.                 *
 *                                                                        *
 * The TYPO3 project - inspiring people to share!                         *
 *                                                                        */

namespace PIPEU\Factura\Delivery\Domain\Model\Provider;

use PIPEU\Factura\Domain\Model\Tax;
use PIPEU\Factura\Domain\Model\Money;
use PIPEU\Factura\Delivery\Domain\Model\AbstractProvider;
use TYPO3\Flow\Utility\Arrays;

/**
 * Class Pickup
 *
 * @package PIPEU\Factura\Delivery\Domain\Model\Provider
 */
class Pickup extends AbstractProvider {

	const TYPE = 'pickup';

	/**
	 * @param mixed $object
	 * @return boolean
	 */
	public function isProvided($object) {
		return TRUE;
	}

	/**
	 * @param mixed $object
	 * @return Money
	 */
	public function getUnitPrice($object) {
		$configuration = $this->getConfiguration();
		return new Money((integer)Arrays::getValueByPath($configuration, 'price'));
	}

	/**
	 * @return Tax
	 */
	public function getTax() {
		$configuration = $this->getConfiguration();
		return new Tax((integer)Arrays::getValueByPath($configuration, 'tax'));
	}
}
