<?php
/*                                                                        *
 * This script belongs to the TYPO3 Flow framework.                       *
 *                                                                        *
 * It is free software; you can redistribute it and/or modify it under    *
 * the terms of the GNU Lesser General Public License, either version 3   *
 * of the License, or (at your option) any later version.                 *
 *                                                                        *
 * The TYPO3 project - inspiring people to share!                         *
 *                                                                        */

namespace PIPEU\Factura\Delivery\Domain\Model\Provider;

use PIPEU\Factura\Delivery\Domain\Model\Box;
use PIPEU\Factura\Delivery\Domain\Model\Exceptions\ObjectNotProvidedException;
use PIPEU\Factura\Domain\Interfaces\InterfaceCountry;
use PIPEU\Factura\Domain\Interfaces\InterfaceFacturaDocument;
use PIPEU\Factura\Domain\Interfaces\InterfaceTotalWeight;
use PIPEU\Geo\Domain\Model\Country;
use PIPEU\Factura\Domain\Model\Tax;
use PIPEU\Factura\Domain\Model\Weight;
use PIPEU\Factura\Domain\Model\Money;
use Doctrine\Common\Collections\ArrayCollection;
use PIPEU\Factura\Delivery\Domain\Model\AbstractProvider;
use TYPO3\Flow\Utility\Arrays;
use PIPEU\Factura\Delivery\Domain\Model\Packaging\Service\Interfaces\PackageFactoryInterface;
use TYPO3\Flow\Annotations as Flow;

/**
 * Class Dhl
 *
 * @package PIPEU\Factura\Delivery\Domain\Model\Provider
 */
class Dhl extends AbstractProvider {

	const TYPE = 'dhl';

	/**
	 * @var PackageFactoryInterface
	 * @Flow\Inject
	 */
	protected $packageFactory;

	/**
	 * @param mixed $object
	 * @return boolean
	 * @throws ObjectNotProvidedException
	 */
	public function isProvided($object) {
		if (!($object instanceof InterfaceTotalWeight)) {
			throw new ObjectNotProvidedException('Object needs to be an implementation of \PIPEU\Factura\Domain\Interfaces\InterfaceTotalWeight', 1403430004);
		}
		if (!($object instanceof InterfaceCountry)) {
			throw new ObjectNotProvidedException('Object needs to be an implementation of \PIPEU\Factura\Domain\Interfaces\InterfaceCountry', 1403430005);
		}
		if (!($object instanceof InterfaceFacturaDocument)) {
			throw new ObjectNotProvidedException('Object needs to be an implementation of \PIPEU\Factura\Domain\Interfaces\InterfaceFacturaDocument', 1415276115);
		}
		if(NULL === $box = $this->packageFactory->findBestPackagingForFacturaItems($object->getFacturaItems())){
			return FALSE;
		}

		/** @var InterfaceCountry $countryInterface */
		$countryInterface = $object;
		$country = $countryInterface->getCountry();

		/** @var InterfaceTotalWeight $totalWeightInterface */
		$totalWeightInterface = $object;
		$weight = $totalWeightInterface->getTotalWeight();

		if (!($weight instanceof Weight)) {
			return FALSE;
		}

		if (!($country instanceof Country)) {
			return FALSE;
		}

		if($box instanceof Box){
			$weight = new Weight($weight->getValue() + $box->getEmptyWeight());
		}

		$priceByCountryAndWeight = $this->getPriceByCountryAndWeight($country, $weight);
		if ($priceByCountryAndWeight === NULL) {
			return FALSE;
		}
		return TRUE;
	}

	/**
	 * @param mixed $object
	 * @throws ObjectNotProvidedException
	 * @return Money
	 */
	public function getUnitPrice($object) {
		if (!($object instanceof InterfaceTotalWeight)) {
			throw new ObjectNotProvidedException('Object needs to be an implementation of \PIPEU\Factura\Domain\Interfaces\InterfaceTotalWeight', 1403430006);
		}
		if (!($object instanceof InterfaceCountry)) {
			throw new ObjectNotProvidedException('Object needs to be an implementation of \PIPEU\Factura\Domain\Interfaces\InterfaceCountry', 1403430007);
		}
		if (!($object instanceof InterfaceFacturaDocument)) {
			throw new ObjectNotProvidedException('Object needs to be an implementation of \PIPEU\Factura\Domain\Interfaces\InterfaceFacturaDocument', 1415277478);
		}
		if(NULL === $box = $this->packageFactory->findBestPackagingForFacturaItems($object->getFacturaItems())){
			throw new ObjectNotProvidedException('No packaging available', 1415277479);
		}

		/** @var InterfaceCountry $countryInterface */
		$countryInterface = $object;
		$country = $countryInterface->getCountry();

		/** @var InterfaceTotalWeight $totalWeightInterface */
		$totalWeightInterface = $object;
		$weight = $totalWeightInterface->getTotalWeight();

		$unitPrice = new Money(0);

		if (!($weight instanceof Weight)) {
			$weight = new Weight(0);
		}

		if($box instanceof Box){
			$unitPrice = new Money($box->getUnitPrice());
			$weight = new Weight($weight->getValue() + $box->getEmptyWeight());
		}

		$deliveryUnitPrice = $this->getPriceByCountryAndWeight($country, $weight);
		if ($deliveryUnitPrice instanceof Money) {
			$unitPrice = new Money($deliveryUnitPrice->getValue() + $unitPrice->getValue());
		}

		return $unitPrice;
	}

	/**
	 * @return Tax
	 */
	public function getTax() {
		$configuration = $this->getConfiguration();
		return new Tax((integer)Arrays::getValueByPath($configuration, 'tax'));
	}

	/**
	 * @param Country $country
	 * @param Weight $weight
	 * @return Money
	 */
	protected function getPriceByCountryAndWeight(Country $country, Weight $weight) {

		$weightPriceScaleByCountry = $this->getWeightPriceScaleByCountry($country);

		$weightFilter = function ($configuration) use ($weight) {
			return (boolean)((integer)(Arrays::getValueByPath($configuration, 'maxWeight')) >= $weight->getValue());
		};

		$weightMatchingPriceScale = $weightPriceScaleByCountry->filter($weightFilter);

		$weightPriceScale = $weightMatchingPriceScale->first();

		if (!is_array($weightPriceScale)) {
			return NULL;
		}

		return new Money((integer)Arrays::getValueByPath($weightPriceScale, 'value'));
	}

	/**
	 * @param Country $country
	 * @return boolean
	 */
	protected function providedCountry(Country $country) {
		return (boolean)$this->getCountryConfiguration($country);
	}

	/**
	 * @param Country $country
	 * @return array
	 */
	protected function getCountryConfiguration(Country $country) {
		$configuration = $this->getConfiguration();
		return Arrays::getValueByPath($configuration, 'countries.' . $country->getIso2());
	}

	/**
	 * @param string $zone
	 * @return array
	 */
	protected function getPriceZoneConfiguration($zone) {
		$configuration = $this->getConfiguration();
		return Arrays::getValueByPath($configuration, 'price.zone.' . $zone);
	}

	/**
	 * @param Country $country
	 * @return ArrayCollection
	 */
	protected function getWeightPriceScaleByCountry(Country $country) {

		$countryConfiguration = $this->getCountryConfiguration($country);

		if (!is_array($countryConfiguration)) {
			return new ArrayCollection();
		}

		$zone = Arrays::getValueByPath($countryConfiguration, 'zone');

		if ($zone === NULL) {
			return new ArrayCollection();
		}

		$weightScale = $this->getPriceZoneConfiguration($zone);

		if (!is_array($weightScale)) {
			return new ArrayCollection();
		}

		return new ArrayCollection($weightScale);
	}
}
