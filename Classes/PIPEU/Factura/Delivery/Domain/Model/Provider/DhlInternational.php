<?php
/*                                                                        *
 * This script belongs to the TYPO3 Flow framework.                       *
 *                                                                        *
 * It is free software; you can redistribute it and/or modify it under    *
 * the terms of the GNU Lesser General Public License, either version 3   *
 * of the License, or (at your option) any later version.                 *
 *                                                                        *
 * The TYPO3 project - inspiring people to share!                         *
 *                                                                        */

namespace PIPEU\Factura\Delivery\Domain\Model\Provider;

use PIPEU\Factura\Delivery\Domain\Model\Box;
use PIPEU\Factura\Delivery\Domain\Model\Exceptions\ObjectNotProvidedException;
use PIPEU\Factura\Domain\Interfaces\InterfaceCountry;
use PIPEU\Factura\Domain\Interfaces\InterfaceFacturaDocument;
use PIPEU\Factura\Domain\Interfaces\InterfaceTotalWeight;
use PIPEU\Geo\Domain\Model\Country;
use PIPEU\Factura\Domain\Model\Tax;
use PIPEU\Factura\Domain\Model\Weight;
use PIPEU\Factura\Domain\Model\Money;
use PIPEU\Factura\Delivery\Domain\Model\AbstractProvider;
use TYPO3\Flow\Utility\Arrays;
use PIPEU\Factura\Delivery\Domain\Model\Packaging\Service\Interfaces\PackageFactoryInterface;
use TYPO3\Flow\Annotations as Flow;

/**
 * Class DhlInternational
 *
 * @package PIPEU\Factura\Delivery\Domain\Model\Provider
 */
class DhlInternational extends AbstractProvider {

	const TYPE = 'dhlInternational';

	/**
	 * @var PackageFactoryInterface
	 * @Flow\Inject
	 */
	protected $packageFactory;

	/**
	 * @param mixed $object
	 * @return boolean
	 * @throws ObjectNotProvidedException
	 */
	public function isProvided($object) {
		if (!($object instanceof InterfaceTotalWeight)) {
			throw new ObjectNotProvidedException('Object needs to be an implementation of \PIPEU\Factura\Domain\Interfaces\InterfaceTotalWeight', 1403430000);
		}
		if (!($object instanceof InterfaceCountry)) {
			throw new ObjectNotProvidedException('Object needs to be an implementation of \PIPEU\Factura\Domain\Interfaces\InterfaceCountry', 1403430001);
		}
		if (!($object instanceof InterfaceFacturaDocument)) {
			throw new ObjectNotProvidedException('Object needs to be an implementation of \PIPEU\Factura\Domain\Interfaces\InterfaceFacturaDocument', 1415276115);
		}
		if(NULL === $box = $this->packageFactory->findBestPackagingForFacturaItems($object->getFacturaItems())){
			return FALSE;
		}

		/** @var InterfaceCountry $countryInterface */
		$countryInterface = $object;
		$country = $countryInterface->getCountry();

		/** @var InterfaceTotalWeight $totalWeightInterface */
		$totalWeightInterface = $object;
		$weight = $totalWeightInterface->getTotalWeight();

		if (!($weight instanceof Weight)) {
			return FALSE;
		}

		if (!($country instanceof Country)) {
			return FALSE;
		}

		if($box instanceof Box){
			$weight = new Weight($weight->getValue() + $box->getEmptyWeight());
		}

		if ($weight->getValue() > $this->getMaxWeight()->getValue()) {
			return FALSE;
		}

		$priceByCountryAndWeight = $this->getPriceByCountryAndWeight($country, $weight);
		if ($priceByCountryAndWeight === NULL) {
			return FALSE;
		}

		return TRUE;
	}

	/**
	 * @param mixed $object
	 * @throws ObjectNotProvidedException
	 * @return Money
	 */
	public function getUnitPrice($object) {
		if (!($object instanceof InterfaceTotalWeight)) {
			throw new ObjectNotProvidedException('Object needs to be an implementation of \PIPEU\Factura\Domain\Interfaces\InterfaceTotalWeight', 1403430002);
		}
		if (!($object instanceof InterfaceCountry)) {
			throw new ObjectNotProvidedException('Object needs to be an implementation of \PIPEU\Factura\Domain\Interfaces\InterfaceCountry', 1403430003);
		}
		if (!($object instanceof InterfaceFacturaDocument)) {
			throw new ObjectNotProvidedException('Object needs to be an implementation of \PIPEU\Factura\Domain\Interfaces\InterfaceFacturaDocument', 1415277478);
		}
		if(NULL === $box = $this->packageFactory->findBestPackagingForFacturaItems($object->getFacturaItems())){
			throw new ObjectNotProvidedException('No packaging available', 1415277479);
		}

		/** @var InterfaceCountry $countryInterface */
		$countryInterface = $object;
		$country = $countryInterface->getCountry();

		/** @var InterfaceTotalWeight $totalWeightInterface */
		$totalWeightInterface = $object;
		$weight = $totalWeightInterface->getTotalWeight();

		$unitPrice = new Money(0);

		if (!($weight instanceof Weight)) {
			$weight = new Weight(0);
		}

		if($box instanceof Box){
			$unitPrice = new Money($box->getUnitPrice());
			$weight = new Weight($weight->getValue() + $box->getEmptyWeight());
		}

		$deliveryUnitPrice = $this->getPriceByCountryAndWeight($country, $weight);
		if ($deliveryUnitPrice instanceof Money) {
			$unitPrice = new Money($deliveryUnitPrice->getValue() + $unitPrice->getValue());
		}

		return $unitPrice;
	}

	/**
	 * @return Tax
	 */
	public function getTax() {
		$configuration = $this->getConfiguration();
		return new Tax((integer)Arrays::getValueByPath($configuration, 'tax'));
	}

	/**
	 * @param Country $country
	 * @param Weight $weight
	 * @return Money
	 */
	protected function getPriceByCountryAndWeight(Country $country, Weight $weight) {

		$priceScaleByCountry = $this->getPriceScaleByCountry($country);

		if (!is_array($priceScaleByCountry)) {
			return NULL;
		}

		$basePrice = new Money((integer)Arrays::getValueByPath($priceScaleByCountry, 'base'));
		$unitPrice = new Money((integer)Arrays::getValueByPath($priceScaleByCountry,
			$this->getEuMemperPropertyPath($country) . '.' . $this->getPricePropertyPath($country)
		));

		$weightCosts = new Money((integer)($unitPrice->getValue() * ceil((string)$weight->getValue() / $weight->getDecimalFactor())));
		if ($weightCosts->getValue() === 0) {
			return NULL;
		}

		$total = new Money((integer)$weightCosts->getValue() + $basePrice->getValue());

		if ($total->getValue() === 0) {
			return NULL;
		}

		return $total;
	}

	/**
	 * @param Country $country
	 * @return string
	 */
	protected function getPricePropertyPath(Country $country) {
		if ($this->forcePremium($country) === TRUE) {
			return 'premium';
		}
		return 'value';
	}

	/**
	 * @param Country $country
	 * @return string
	 */
	protected function getEuMemperPropertyPath(Country $country) {
		if ($country->getEuMember() === TRUE) {
			return 'euMember';
		}
		return 'nonEuMember';
	}

	/**
	 * @param Country $country
	 * @return boolean
	 */
	protected function forcePremium(Country $country) {
		$countryConfiguration = $this->getCountryConfiguration($country);
		if (!is_array($countryConfiguration)) {
			return FALSE;
		}
		return (boolean)Arrays::getValueByPath($countryConfiguration, 'forcePremium');
	}

	/**
	 * @param Country $country
	 * @return array
	 */
	protected function getPriceScaleByCountry(Country $country) {
		$zone = $this->getCountryZone($country);
		if ($zone === NULL) {
			return NULL;
		}
		return $this->getPriceZoneConfiguration($zone);
	}

	/**
	 * @param Country $country
	 * @return integer
	 */
	protected function getCountryZone(Country $country) {
		$configuration = $this->getCountryConfiguration($country);
		if (!is_array($configuration)) {
			return NULL;
		}
		return Arrays::getValueByPath($configuration, 'zone');
	}

	/**
	 * @param Country $country
	 * @return array
	 */
	protected function getCountryConfiguration(Country $country) {
		$configuration = $this->getConfiguration();
		return Arrays::getValueByPath($configuration, 'countries.' . $country->getIso2());
	}

	/**
	 * @param string $zone
	 * @return array
	 */
	protected function getPriceZoneConfiguration($zone) {
		$configuration = $this->getConfiguration();
		return Arrays::getValueByPath($configuration, 'price.zone.' . $zone);
	}

	/**
	 * @return Weight
	 */
	protected function getMaxWeight() {
		$configuration = $this->getConfiguration();
		return new Weight((integer)Arrays::getValueByPath($configuration, 'maxWeight'));
	}
}
