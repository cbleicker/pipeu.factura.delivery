<?php
/*                                                                        *
 * This script belongs to the TYPO3 Flow framework.                       *
 *                                                                        *
 * It is free software; you can redistribute it and/or modify it under    *
 * the terms of the GNU Lesser General Public License, either version 3   *
 * of the License, or (at your option) any later version.                 *
 *                                                                        *
 * The TYPO3 project - inspiring people to share!                         *
 *                                                                        */

namespace PIPEU\Factura\Delivery\Domain\Model;

use DVDoug\BoxPacker\Box as BoxInterface;

/**
 * Class Box
 *
 * @package PIPEU\Factura\Delivery\Domain\Model
 */
class Box implements BoxInterface {

	/**
	 * @var integer
	 */
	protected $unitPrice;

	/**
	 * @param string $aReference
	 * @param integer $aOuterWidth
	 * @param integer $aOuterLength
	 * @param integer $aOuterDepth
	 * @param integer $aEmptyWeight
	 * @param integer $aInnerWidth
	 * @param integer $aInnerLength
	 * @param integer $aInnerDepth
	 * @param integer $aMaxWeight
	 * @param integer $unitPrice
	 */
	public function __construct($aReference, $aOuterWidth, $aOuterLength, $aOuterDepth, $aEmptyWeight, $aInnerWidth, $aInnerLength, $aInnerDepth, $aMaxWeight, $unitPrice) {
		$this->reference = (string)$aReference;
		$this->outerWidth = (integer)$aOuterWidth;
		$this->outerLength = (integer)$aOuterLength;
		$this->outerDepth = (integer)$aOuterDepth;
		$this->emptyWeight = (integer)$aEmptyWeight;
		$this->innerWidth = (integer)$aInnerWidth;
		$this->innerLength = (integer)$aInnerLength;
		$this->innerDepth = (integer)$aInnerDepth;
		$this->maxWeight = (integer)$aMaxWeight;
		$this->unitPrice = (integer)$unitPrice;
		$this->innerVolume = (integer)($this->innerWidth * $this->innerLength * $this->innerDepth);
	}

	/**
	 * @return string
	 */
	public function getReference() {
		return $this->reference;
	}

	/**
	 * @return integer
	 */
	public function getOuterWidth() {
		return $this->outerWidth;
	}

	/**
	 * @return integer
	 */
	public function getOuterLength() {
		return $this->outerLength;
	}

	/**
	 * @return integer
	 */
	public function getOuterDepth() {
		return $this->outerDepth;
	}

	/**
	 * @return integer
	 */
	public function getEmptyWeight() {
		return $this->emptyWeight;
	}

	/**
	 * @return integer
	 */
	public function getInnerWidth() {
		return $this->innerWidth;
	}

	/**
	 * @return integer
	 */
	public function getInnerLength() {
		return $this->innerLength;
	}

	/**
	 * @return integer
	 */
	public function getInnerDepth() {
		return $this->innerDepth;
	}

	/**
	 * @return integer
	 */
	public function getInnerVolume() {
		return $this->innerVolume;
	}

	/**
	 * @return integer
	 */
	public function getMaxWeight() {
		return $this->maxWeight;
	}

	/**
	 * @return string
	 */
	public function getTitle() {
		return $this->reference;
	}

	/**
	 * @return integer
	 */
	public function getUnitPrice() {
		return $this->unitPrice;
	}
}
