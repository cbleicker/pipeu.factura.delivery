<?php
/*                                                                        *
 * This script belongs to the TYPO3 Flow framework.                       *
 *                                                                        *
 * It is free software; you can redistribute it and/or modify it under    *
 * the terms of the GNU Lesser General Public License, either version 3   *
 * of the License, or (at your option) any later version.                 *
 *                                                                        *
 * The TYPO3 project - inspiring people to share!                         *
 *                                                                        */

namespace PIPEU\Factura\Delivery\Domain\Model;

use TYPO3\Flow\Utility\Arrays;

/**
 * Class AbstractProvider
 *
 * @package PIPEU\Factura\Delivery\Domain\Model
 */
abstract class AbstractProvider implements InterfaceProvider {

	const PRIORITY = 1, TYPE = NULL;

	/**
	 * @var array
	 */
	protected $configuration;

	/**
	 * @return integer
	 */
	public function getPriority() {
		return static::PRIORITY;
	}

	/**
	 * @return string
	 */
	public function getType() {
		return static::TYPE;
	}

	/**
	 * @return string
	 */
	public function getTitle() {
		$configuration = $this->getConfiguration();
		if (!is_array($configuration)) {
			return NULL;
		}
		return Arrays::getValueByPath($configuration, 'title');
	}

	/**
	 * @return string
	 */
	public function getSubtitle() {
		$configuration = $this->getConfiguration();
		if (!is_array($configuration)) {
			return NULL;
		}
		return Arrays::getValueByPath($configuration, 'subtitle');
	}

	/**
	 * @param array $configuration
	 * @return $this
	 */
	public function setConfiguration(array $configuration = NULL) {
		$this->configuration = $configuration;
	}

	/**
	 * @return array
	 */
	public function getConfiguration() {
		return $this->configuration;
	}
}
