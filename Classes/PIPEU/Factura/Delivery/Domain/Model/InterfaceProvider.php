<?php
/*                                                                        *
 * This script belongs to the TYPO3 Flow framework.                       *
 *                                                                        *
 * It is free software; you can redistribute it and/or modify it under    *
 * the terms of the GNU Lesser General Public License, either version 3   *
 * of the License, or (at your option) any later version.                 *
 *                                                                        *
 * The TYPO3 project - inspiring people to share!                         *
 *                                                                        */

namespace PIPEU\Factura\Delivery\Domain\Model;

use PIPEU\Factura\Domain\Model\Tax;
use PIPEU\Factura\Domain\Model\Money;

/**
 * Interface InterfaceProvider
 *
 * @package PIPEU\Factura\Delivery\Domain\Model
 */
interface InterfaceProvider {

	/**
	 * @return string
	 */
	public function getTitle();

	/**
	 * @return string
	 */
	public function getSubtitle();

	/**
	 * @param array $configuration
	 * @return $this
	 */
	public function setConfiguration(array $configuration = array());

	/**
	 * @return array
	 */
	public function getConfiguration();

	/**
	 * @param mixed $object
	 * @return boolean
	 */
	public function isProvided($object);

	/**
	 * @param mixed $object
	 * @return Money
	 */
	public function getUnitPrice($object);

	/**
	 * @return Tax
	 */
	public function getTax();

	/**
	 * @return integer
	 */
	public function getPriority();

	/**
	 * @return string
	 */
	public function getType();

}
