<?php
/*                                                                        *
 * This script belongs to the TYPO3 Flow framework.                       *
 *                                                                        *
 * It is free software; you can redistribute it and/or modify it under    *
 * the terms of the GNU Lesser General Public License, either version 3   *
 * of the License, or (at your option) any later version.                 *
 *                                                                        *
 * The TYPO3 project - inspiring people to share!                         *
 *                                                                        */

namespace PIPEU\Factura\Delivery;

use TYPO3\Flow\Configuration\ConfigurationManager;
use TYPO3\Flow\Package\Package as BasePackage;
use TYPO3\Flow\Package\PackageManagerInterface;
use TYPO3\Flow\Package\PackageInterface;
use TYPO3\Flow\Monitor\FileMonitor;
use TYPO3\Flow\Core\Booting\Step;
use TYPO3\Flow\Core\Bootstrap;

/**
 * Class Package
 *
 * @package PIPEU\Factura\Delivery
 */
class Package extends BasePackage {

	/**
	 * @param Bootstrap $bootstrap
	 * @return void
	 */
	public function boot(Bootstrap $bootstrap) {
		$dispatcher = $bootstrap->getSignalSlotDispatcher();

		$dispatcher->connect('TYPO3\Flow\Configuration\ConfigurationManager', 'configurationManagerReady', function (ConfigurationManager $configurationManager) {
			$configurationManager->registerConfigurationType('Delivery', ConfigurationManager::CONFIGURATION_PROCESSING_TYPE_DEFAULT, TRUE);
		});

		$context = $bootstrap->getContext();
		if (!$context->isProduction()) {
			$dispatcher->connect('TYPO3\Flow\Core\Booting\Sequence', 'afterInvokeStep', function (Step $step) use ($bootstrap) {
				if ($step->getIdentifier() === 'typo3.flow:systemfilemonitor') {
					/** @var FileMonitor $deliveryProviderConfigurationFileMonitor */
					$deliveryProviderConfigurationFileMonitor = FileMonitor::createFileMonitorAtBoot('PIPEU-Factura-Delivery-Configuration', $bootstrap);
					/** @var PackageManagerInterface $packageManager */
					$packageManager = $bootstrap->getEarlyInstance('TYPO3\Flow\Package\PackageManagerInterface');
					/** @var PackageInterface $package */
					foreach ($packageManager->getActivePackages() as $packageKey => $package) {
						if ($packageManager->isPackageFrozen($packageKey)) {
							continue;
						}
						if (file_exists($package->getConfigurationPath())) {
							$deliveryProviderConfigurationFileMonitor->monitorDirectory($package->getConfigurationPath(), 'Delivery(\..+)\.yaml');
						}
					}
					$deliveryProviderConfigurationFileMonitor->monitorDirectory(FLOW_PATH_CONFIGURATION, 'Delivery(\..+)\.yaml');
					$deliveryProviderConfigurationFileMonitor->detectChanges();
					$deliveryProviderConfigurationFileMonitor->shutdownObject();
				}
			});
		}
	}
}
