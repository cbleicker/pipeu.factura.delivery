<?php
/*                                                                        *
 * This script belongs to the TYPO3 Flow framework.                       *
 *                                                                        *
 * It is free software; you can redistribute it and/or modify it under    *
 * the terms of the GNU Lesser General Public License, either version 3   *
 * of the License, or (at your option) any later version.                 *
 *                                                                        *
 * The TYPO3 project - inspiring people to share!                         *
 *                                                                        */

namespace PIPEU\Factura\Delivery\Tests\Functional\Fixtures\Domain\Model;

use DVDoug\BoxPacker\Item as ItemInterface;

/**
 * Class TestItem
 *
 * @package PIPEU\Factura\Delivery\Tests\Functional\Fixtures\Domain\Model
 */
class TestItem implements ItemInterface {

	/**
	 * @param string $aDescription
	 * @param integer $aWidth
	 * @param integer $aLength
	 * @param integer $aDepth
	 * @param integer $aWeight
	 */
	public function __construct($aDescription, $aWidth, $aLength, $aDepth, $aWeight) {
		$this->description = $aDescription;
		$this->width = $aWidth;
		$this->length = $aLength;
		$this->depth = $aDepth;
		$this->weight = $aWeight;
		$this->volume = $this->width * $this->length * $this->depth;
	}

	/**
	 * @return string
	 */
	public function getDescription() {
		return $this->description;
	}

	/**
	 * @return integer
	 */
	public function getWidth() {
		return $this->width;
	}

	/**
	 * @return integer
	 */
	public function getLength() {
		return $this->length;
	}

	/**
	 * @return integer
	 */
	public function getDepth() {
		return $this->depth;
	}

	/**
	 * @return integer
	 */
	public function getWeight() {
		return $this->weight;
	}

	/**
	 * @return integer
	 */
	public function getVolume() {
		return $this->volume;
	}
}
