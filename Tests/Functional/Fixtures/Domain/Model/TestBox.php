<?php
/*                                                                        *
 * This script belongs to the TYPO3 Flow framework.                       *
 *                                                                        *
 * It is free software; you can redistribute it and/or modify it under    *
 * the terms of the GNU Lesser General Public License, either version 3   *
 * of the License, or (at your option) any later version.                 *
 *                                                                        *
 * The TYPO3 project - inspiring people to share!                         *
 *                                                                        */

namespace PIPEU\Factura\Delivery\Tests\Functional\Fixtures\Domain\Model;

use DVDoug\BoxPacker\Box as BoxInterface;

/**
 * Class TestBox
 *
 * @package PIPEU\Factura\Delivery\Tests\Functional\Fixtures\Domain\Model
 */
class TestBox implements BoxInterface {

	/**
	 * @param string $aReference
	 * @param integer $aOuterWidth
	 * @param integer $aOuterLength
	 * @param integer $aOuterDepth
	 * @param integer $aEmptyWeight
	 * @param integer $aInnerWidth
	 * @param integer $aInnerLength
	 * @param integer $aInnerDepth
	 * @param integer $aMaxWeight
	 */
	public function __construct($aReference, $aOuterWidth, $aOuterLength, $aOuterDepth, $aEmptyWeight, $aInnerWidth, $aInnerLength, $aInnerDepth, $aMaxWeight) {
		$this->reference = $aReference;
		$this->outerWidth = $aOuterWidth;
		$this->outerLength = $aOuterLength;
		$this->outerDepth = $aOuterDepth;
		$this->emptyWeight = $aEmptyWeight;
		$this->innerWidth = $aInnerWidth;
		$this->innerLength = $aInnerLength;
		$this->innerDepth = $aInnerDepth;
		$this->maxWeight = $aMaxWeight;
		$this->innerVolume = $this->innerWidth * $this->innerLength * $this->innerDepth;
	}

	/**
	 * @return string
	 */
	public function getReference() {
		return $this->reference;
	}

	/**
	 * @return integer
	 */
	public function getOuterWidth() {
		return $this->outerWidth;
	}

	/**
	 * @return integer
	 */
	public function getOuterLength() {
		return $this->outerLength;
	}

	/**
	 * @return integer
	 */
	public function getOuterDepth() {
		return $this->outerDepth;
	}

	/**
	 * @return integer
	 */
	public function getEmptyWeight() {
		return $this->emptyWeight;
	}

	/**
	 * @return integer
	 */
	public function getInnerWidth() {
		return $this->innerWidth;
	}

	/**
	 * @return integer
	 */
	public function getInnerLength() {
		return $this->innerLength;
	}

	/**
	 * @return integer
	 */
	public function getInnerDepth() {
		return $this->innerDepth;
	}

	/**
	 * @return integer
	 */
	public function getInnerVolume() {
		return $this->innerVolume;
	}

	/**
	 * @return integer
	 */
	public function getMaxWeight() {
		return $this->maxWeight;
	}
}
