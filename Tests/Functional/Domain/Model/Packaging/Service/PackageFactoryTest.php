<?php
/*                                                                        *
 * This script belongs to the TYPO3 Flow framework.                       *
 *                                                                        *
 * It is free software; you can redistribute it and/or modify it under    *
 * the terms of the GNU Lesser General Public License, either version 3   *
 * of the License, or (at your option) any later version.                 *
 *                                                                        *
 * The TYPO3 project - inspiring people to share!                         *
 *                                                                        */

namespace PIPEU\Factura\Delivery\Tests\Functional\Domain\Model\Packaging\Service;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use DVDoug\BoxPacker\ItemList;
use PIPEU\Factura\Delivery\Tests\Functional\Fixtures\Domain\Model\TestBox;
use PIPEU\Factura\Delivery\Tests\Functional\Fixtures\Domain\Model\TestItem;
use TYPO3\Flow\Tests\FunctionalTestCase as TestCase;
use PIPEU\Factura\Delivery\Domain\Model\Packaging\Service\Interfaces\PackageFactoryInterface;

/**
 * Class PackageFactoryTest
 *
 * @package PIPEU\Factura\Delivery\Tests\Functional\Domain\Model\Packaging\Service
 */
class PackageFactoryTest extends TestCase {

	/**
	 * @var PackageFactoryInterface
	 */
	protected $packageFactory;

	/**
	 */
	public function setUp() {
		parent::setUp();
		$this->packageFactory = $this->objectManager->get('PIPEU\Factura\Delivery\Domain\Model\Packaging\Service\Interfaces\PackageFactoryInterface');
	}

	/**
	 * @test
	 */
	public function dummyFitTest() {
		$testBoxes = $this->getTestBoxCollection();

		$testItemList = new ItemList();
		$testItemList->insert($this->getDummy());
		$expected = $testItemList->count();
		$packedBox = $this->packageFactory->packIt($testBoxes->first(), $testItemList);
		$this->assertEquals($expected, $packedBox->getItems()->count(), '1 Dummy fit to Maxibrief');

		$testItemList = new ItemList();
		$testItemList->insert($this->getDummy());
		$testItemList->insert($this->getDummy());
		$testItemList->insert($this->getDummy());
		$testItemList->insert($this->getDummy());
		$testItemList->insert($this->getDummy());
		$testItemList->insert($this->getDummy());
		$testItemList->insert($this->getDummy());
		$testItemList->insert($this->getDummy());
		$expected = $testItemList->count();
		$packedBox = $this->packageFactory->packIt($testBoxes->first(), $testItemList);
		$this->assertLessThan($expected, $packedBox->getItems()->count(), '8 Dummies does not fit to Maxibrief');
	}

	/**
	 * @test
	 */
	public function setFitTest() {
		$testBoxes = $this->getTestBoxCollection();

		$testItemList = new ItemList();
		$testItemList->insert($this->getSet());
		$expected = $testItemList->count();
		$packedBox = $this->packageFactory->packIt($testBoxes->first(), $testItemList);
		$this->assertEquals($expected, $packedBox->getItems()->count(), '1 Set fit to Maxibrief');

		$testItemList = new ItemList();
		$testItemList->insert($this->getSet());
		$testItemList->insert($this->getSet());
		$expected = $testItemList->count();
		$packedBox = $this->packageFactory->packIt($testBoxes->first(), $testItemList);
		$this->assertLessThan($expected, $packedBox->getItems()->count(), '2 Sets does not fit to Maxibrief');
	}

	/**
	 * @test
	 */
	public function bagFitTest() {
		$testBoxes = $this->getTestBoxCollection();

		$testItemList = new ItemList();
		$testItemList->insert($this->getBag());
		$expected = $testItemList->count();
		$packedBox = $this->packageFactory->packIt($testBoxes->first(), $testItemList);
		$this->assertEquals($expected, $packedBox->getItems()->count(), '1 Bag fit to Maxibrief');

		$testItemList = new ItemList();
		$testItemList->insert($this->getBag());
		$testItemList->insert($this->getBag());
		$testItemList->insert($this->getBag());
		$testItemList->insert($this->getBag());
		$testItemList->insert($this->getBag());
		$testItemList->insert($this->getBag());
		$expected = $testItemList->count();
		$packedBox = $this->packageFactory->packIt($testBoxes->first(), $testItemList);
		$this->assertLessThan($expected, $packedBox->getItems()->count(), '6 Bags does not fit to Maxibrief');
	}

	/**
	 * @test
	 */
	public function bagSetFitTest() {
		$testBoxes = $this->getTestBoxCollection();

		$testItemList = new ItemList();
		$testItemList->insert($this->getBag());
		$testItemList->insert($this->getSet());
		$expected = $testItemList->count();
		$packedBox = $this->packageFactory->packIt($testBoxes->first(), $testItemList);
		$this->assertEquals($expected, $packedBox->getItems()->count(), '1 Bag and 1 Set fit to Maxibrief');

		$testItemList = new ItemList();
		$testItemList->insert($this->getBag());
		$testItemList->insert($this->getBag());
		$testItemList->insert($this->getSet());
		$expected = $testItemList->count();
		$packedBox = $this->packageFactory->packIt($testBoxes->first(), $testItemList);
		$this->assertLessThan($expected, $packedBox->getItems()->count(), '2 Bags and 1 Set does not fit to Maxibrief');

		$testItemList = new ItemList();
		$testItemList->insert($this->getBag());
		$testItemList->insert($this->getBag());
		$testItemList->insert($this->getSet());
		$expected = $testItemList->count();
		$packedBox = $this->packageFactory->packIt($testBoxes->next(), $testItemList);
		$this->assertEquals($expected, $packedBox->getItems()->count(), '2 Bags and 1 Set fits in XXL Paket');

	}

	/**
	 * @return TestItem
	 */
	protected function getDummy() {
		return new TestItem('Dummy', 250, 30, 30, 110);
	}

	/**
	 * @return TestItem
	 */
	protected function getBag() {
		return new TestItem('Bag', 310, 140, 10, 80);
	}

	/**
	 * @return TestItem
	 */
	protected function getSet() {
		return new TestItem('Set', 310, 140, 40, 410);
	}

	/**
	 * @return Collection
	 */
	protected function getTestBoxCollection() {
		return new ArrayCollection(array(
			new TestBox('Maxibrief', 329, 235, 60, 10, 319, 225, 50, 1000),
			new TestBox('Box-XXL', 1200, 600, 600, 400, 1190, 590, 590, 3150)
		));
	}
}
